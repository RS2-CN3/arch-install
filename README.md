# Arch standard installation with no encryption

# WiFI 

## Find Device
`ip a`

Typically it's wlan0 or wlp2s0, then

`iwtcl`

prompt will change to [iwd], replace device and SSID accordingly. 

`station device scan`

`station device get-networks`

`station device connect SSID`

# User ethernet or USB tethering for internet
```ping archlinux.org```

# edit mirror list 

```nano /etc/pacman.d/mirrorlist```

use the server closest to you 

CTRL+k to cut 
CTRL+u to paste 

if china is closest to you 
cut the line below 
``` ## China``` to the top 

# Update the db
```pacman -Syyy```

# Partition 

do 
```
lsblk
```

you will get 
```
sda      8:0    0 931.5G  0 disk 
|-sda1   8:1    0   512M  0 part 
|-sda2   8:2    0   100G  0 part /
`-sda3   8:3    0   831G  0 part /home
sdb      8:16   1   7.4G  0 disk 
|-sdb1   8:17   1 719.3M  0 part /run/media/sushant/MANJARO_ARCHITECT_2003
`-sdb2   8:18   1     4M  0 part 
sr0     11:0    1  1024M  0 rom  
``` 
as output 

the device we want is /dev/sda (it could be sdc sdb for you)

## Partiion using fdisk

```
fdisk /dev/sda
```

you will get 
```
Welcome to fdisk (util-linux 2.36).
Changes will remain in memory only, until you decide to write them.
Be careful before using the write command.


Command (m for help): 

```

do the following 

create new gpt table and make new Partiion. we will create 3 partiion
one for efi (512M) one for root(100G) and one for home (you can adjust the size accordingly)
do the following in order 
```g```

```n```
set default for first two entry (partiion number and first sector)
for last sector 
```+512M```

```t```

```1```

root parition

```n```

set default for first two entry (partiion number and first sector)
for last sector 

```+100G```

for home parition 

```n```

press enter for all three entries 

press 'p' to print new paritions 

```
Device         Start        End    Sectors  Size Type
/dev/sda1       2048    1050623    1048576  512M EFI System
/dev/sda2    1050624  210765823  209715200  100G Linux filesystem
/dev/sda3  210765824 1953525134 1742759311  831G Linux filesystem
```

you should have something like this


```w``` to write 
this will format everything 

## Formating 

do the following 

```mkfs.fat -F32 /dev/sda1```

```mkfs.ext4 /dev/sda2```

```mkfs.ext4 /dev/sda3```


## Mount 

```mount /dev/sda2 /mnt```

```mkdir /mnt/home```

```mount /dev/sda3 /mnt/home```


## Generating fstab

```mkdir /mnt/etc```

```genfstab -U -p /mnt >> /mnt/etc/fstab```

to verify 

```cat /mnt/etc/fstab```

your output should be:
```
# /dev/sda2
UUID=d22ff8b2-07c6-43bf-bb0e-a33966b2a00c       /               ext4            rw,relatime     0 1

# /dev/sda3
UUID=ab7f1e4c-028b-4f52-b8c0-1ced5d9a1f05       /home           ext4            rw,relatime     0 2
```

# Installing base packages

```pacstrap -i /mnt base linux linux-headers linux-firmware``` 

use chroot command 
```arch-chroot /mnt```

now,
```pacman -S nano base-devel dialog --needed```

## FOr networking 

```pacman -S networkmanager wpa_supplicant wireless_tools netctl```

to enable network at boot 

```systemctl enable NetworkManager```

# Generate ramdisk

```mkinitcpio -p linux```

# Locale

```nano /etc/locale.gen```

here below 
```Examples: 
en_US .... 
```
uncomment (remove #) of both lines
```ctrl+o``` to save

```ctrl+x``` to exit

do
```locale-gen```

# Users 
password for root 
```passwd```

create users
```useradd -m -g users -G wheel <user_name>```
password for user 
```passwd <user_name>```

# Access to sudo 

```EDITOR=nano visudo```

scroll below and uncomment 

```%wheel ALL=(ALL) ALL```

you can uncomment the line with nopasswd if you dont want to enter password everyhing you run as root

ctrl+o and ctrl +x to save and exit

# Setting up grub

```pacman -S grub efibootmgr dosfstools os-prober mtools```

```mkdir /boot/EFI```

```mount /dev/sda1 /boot/EFI```

```grub-install --target=x86_64-efi --bootloader-id=grub_uefi --recheck```

```mkdir /boot/grub/locale```



```cp /usr/share/locale/en\@quot/LC_MESSAGES/grub.mo /boot/grub/locale/en.mo```

```grub-mkconfig -o /boot/grub/grub.cfg```

# Additionals 

```pacman -S intel-ucode``` for intel amd-ucode for amd
```pacman -S xorg-server```

## Desktop envirnoment

```pacman -S plasma sddm konsole```

```systemctl enable sddm```

# exit
```exit```
```umount -a```
```poweroff``` 

You are good to go

### my recommendation after installaion 

install yay 

```pacman -S git```

```git clone https://aur.archlinux.org/yay.git```

```cd yay```

```makepkg -si```

file-manager 

```pacman -S dolphin dolphin-plugins```

video player and image viewer 

```pacman -S mpv nomacs```

web-browser 

```pacman -S firefox```

```yay -S brave-bin```

office suite 

```pacman -S libreoffice-still```
or
```pacman -S libreoffice-fresh``` for latest version (might be buggy though)

text editors 

```pacman -S kate code```

torrent application 

```pacman -S qbittorrent```

checkout [learnlinux](https://www.youtube.com/watch?v=a00wbjy2vns)







